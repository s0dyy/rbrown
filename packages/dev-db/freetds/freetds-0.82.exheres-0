# Copyright 2008 Richard Brown
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'freetds-0.64.ebuild' from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require freetds

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="odbc msdblib"

DEPENDENCIES="
    build+run:
        odbc? ( dev-db/unixODBC )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}/${PNV}-blk-version.patch"
)

src_configure() {
    econf \
        --hates=docdir \
        --hates=datarootdir \
        --sysconfdir=/etc/${PN} \
        $(option_enable msdblib) \
        $(option_enable odbc) \
        $(option odbc && echo --with-unixodbc=/usr)
}

src_test() {
    # most of these tests require a server to test with

    # skipping following tests from src/tds
    # dataread dynamic1 t000{1-6} utf8_1 utf8_2 utf8_3
    emake -C src/tds check TESTS="convert iconv_fread numeric t0007 t0008"

    # also skipping all the units in other src srcsubdirs
}

pkg_postinst() {
    #Formatting intentional
    if option odbc; then
        echo "[FreeTDS]
Description = FreeTDS unixODBC Driver
Driver = /usr/${LIBDIR}/libtdsodbc.so.0
Setup = /usr/${LIBDIR}/libtdsodbc.so.0" | odbcinst -i -d -r \
             || eerror "Failed to add FreeTDS with odbcinst"
        echo "[SQL Server]
Description = FreeTDS unixODBC Driver
Driver = /usr/${LIBDIR}/libtdsodbc.so.0
Setup = /usr/${LIBDIR}/libtdsodbc.so.0" | odbcinst -i -d -r  \
             || eerror "Failed to add SQL Server with odbcinst"
    fi
}

pkg_postrm() {
    if option odbc; then
        odbcinst -u -d -n 'FreeTDS' || eerror "Failed to remove FreeTDS with odbcinst"
        odbcinst -u -d -n 'SQL Server' || eerror "Failed to remove SQL Server with odbcinst"
    fi
}

