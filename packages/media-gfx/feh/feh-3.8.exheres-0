# Copyright 2008 Richard Brown
# Copyright 2011 Ingmar Vanhassel
# Copyright 2011 Maxime Coste
# Copyright 2012 Luis Aranguren <mercurytoxic@luisaranguren.com>
# Distributed under the terms of the GNU General Public License v2

require freedesktop-desktop gtk-icon-cache

SUMMARY="An image viewer at heart, though it does other cool stuff"
HOMEPAGE="https://${PN}.finalrewind.org/"
DOWNLOADS="${HOMEPAGE}${PNV}.tar.bz2"
UPSTREAM_CHANGELOG="https://git.finalrewind.org/${PN}/plain/ChangeLog"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="curl exif xinerama"

DEPENDENCIES="
    build:
        x11-libs/libXt
    build+run:
        media-libs/imlib2[X]
        media-libs/libpng:=
        x11-libs/libX11
        curl? ( net-misc/curl [[ description = [ Add http/ftp support ] ]] )
        exif? ( media-libs/libexif )
        xinerama? ( x11-libs/libXinerama )
    suggestion:
        media-gfx/ImageMagick [[
            description = [ Adds limited support for filetypes supported by ImageMagick ]
        ]]
"

# Tests require a few perl modules we've not packaged, like Test::Command and X11::GUITest
RESTRICT="test"

DEFAULT_SRC_INSTALL_PARAMS=(
    DESTDIR="${IMAGE}"
    PREFIX=/usr
    bin_dir="${IMAGE}"/usr/$(exhost --target)/bin
)

src_prepare() {
    edo sed -re "/(doc_dir|example_dir)/s:${PN}:${PNVR}:" -i config.mk
}

my_enable() { echo ${1}=$(option ${1} 1 0); }

src_compile() {
    emake PREFIX=/usr \
        $(my_enable curl) \
        $(my_enable exif) \
        $(my_enable xinerama)
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

